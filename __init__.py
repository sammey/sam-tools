bl_info = {
	"name": "Sam Tools",
	"blender": (2,80,0),
	"category": "Object",
    "version": (0, 0, 2),
    "description": "Some handy tools for easy FBX export, forcing 1,1,1 scale etc."
}

import bpy
import io_scene_fbx.export_fbx_bin
import bmesh
from math import pi

class FakeOp:
	def report(self, tp, msg):
		print("%s: %s" % (tp, msg))

def correct_transforms(caller, context):
        for obj in bpy.data.objects:
            if obj.type == 'MESH' or obj.type == 'CURVE':
                fix_rotations = (bpy.context.scene.auto_fix_rotations and obj.parent is None)
                fix_scales = bpy.context.scene.auto_fix_scales
                
                caller.report({'INFO'}, "Correcting scale on " + obj.name)
                if fix_rotations:
                    caller.report({'INFO'}, "Correcting rotation on root object " + obj.name)
                    
                obj.select_set(True)
                bpy.context.view_layer.objects.active = obj
                obj.rotation_euler = (obj.rotation_euler[0]+(pi * -90 / 180),obj.rotation_euler[1],obj.rotation_euler[2])
                bpy.ops.object.transform_apply(
                    location = False, 
                    scale = fix_scales, 
                    rotation = fix_rotations
                    )
                obj.rotation_euler = (obj.rotation_euler[0]+(pi * 90 / 180),obj.rotation_euler[1],obj.rotation_euler[2]) 
                obj.select_set(False)
        return {'FINISHED'}
    
def save_fbx(caller, context):
        bpy.ops.object.mode_set(mode='OBJECT')

        if bpy.context.scene.auto_export_fbx: 
            correct_transforms(caller,context)
            
            #only select interesting objects
            for obj in bpy.data.objects:
                if obj.type == 'MESH' or obj.type == 'CURVE' or obj.type == 'ARMATURE':
                    obj.select_set(True)
            
            kwargs = io_scene_fbx.export_fbx_bin.defaults_unity3d()
            
            #get absolute path to your folder (can be folder in hierarchy)
            abspath = bpy.path.abspath(context.scene.fbx_foldername)
                    
            #get our .blend path and replace blend with fbx
            filenamefbx = bpy.path.basename(bpy.context.blend_data.filepath).replace(".blend",".fbx")
            io_scene_fbx.export_fbx_bin.save(FakeOp(),bpy.context,use_selection=True,apply_scale_options='FBX_SCALE_UNITS',filepath=abspath+filenamefbx)
            if caller is not None:
                caller.report({'INFO'}, "Exported " + abspath+filenamefbx)
        return {'FINISHED'}

class SamTools(bpy.types.Panel):
    bl_label = "Sam Tools"
    bl_idname = "OBJECT_PT_SamTools"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Item"
        
    def draw(self, context):
        layout = self.layout
        
        row = layout.row()
        row.prop(context.scene,"fbx_foldername")
        row = layout.row()
        row.prop(context.scene,"auto_fix_rotations")
        row = layout.row()
        row.prop(context.scene,"auto_fix_scales")
        row = layout.row()
        row.prop(context.scene,"auto_export_fbx")
        row = layout.row()
        row.operator("button.directfbx",icon='FILE')
        row = layout.row()
        row.operator("button.fixrotationandscaleforunity",icon='EMPTY_DATA')

class SamTools_PT_MaterialColorPanel(bpy.types.Panel):
    bl_label = "Sam Tools vertex bake color"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "data"

    def color_get(self):
        print("Getting color")
        return self["vertex_fill_color"]


    def color_update(self, context):
        col_R=self.vertex_fill_color[0]
        col_G=self.vertex_fill_color[1]
        col_B=self.vertex_fill_color[2]
        
        bpy.ops.object.mode_set(mode='VERTEX_PAINT')
        bpy.context.object.data.use_paint_mask = True
        bpy.data.brushes["Draw"].color =(col_R,col_G,col_B)
        bpy.ops.paint.vertex_color_set()
        bpy.ops.object.mode_set(mode='EDIT')
            
    bpy.types.Scene.vertex_fill_color=bpy.props.FloatVectorProperty(   
                                            name="Vertex bake color", 
                                            subtype = "COLOR", 
                                            default = [1.0,0.0,0.0],
                                            update=color_update                                   
                                            )            
    def execute(self,context):
        return{"FINISHED"}

    def draw(self, context): 
        self.layout.prop(context.scene,"vertex_fill_color")

class DirectlyToFBX(bpy.types.Operator):
    bl_idname = "button.directfbx"
    bl_label = "Export FBX"
    bl_options = {'REGISTER', 'UNDO'}
    
    def execute(self, context):
        save_fbx(self,context)
        return {'FINISHED'}

class FixRotationAndScaleForUnity(bpy.types.Operator):
    bl_idname = "button.fixrotationandscaleforunity"
    bl_label = "Fix rotation and scale for Unity"

    def execute(self, context):
        correct_transforms(self,context)
        return {'FINISHED'}

def register():
    bpy.types.Scene.fbx_foldername = bpy.props.StringProperty(
            name="FBX relative target folder",
            description="The relative path to the FBX file",
            default="//",
            subtype = 'DIR_PATH'
            )  
    bpy.types.Scene.auto_fix_scales = bpy.props.BoolProperty(
            name="Apply 1,1,1 scales on export",
            description="Fix the scales and rotations for Unity3D",
            default = True
            ) 
    bpy.types.Scene.auto_fix_rotations = bpy.props.BoolProperty(
            name="Fix Unity3D rotations on export",
            description="Apply 1,1,1, scales on export",
            default = True
            ) 
    bpy.types.Scene.auto_export_fbx = bpy.props.BoolProperty(
            name="Auto FBX export on save",
            description="Automatically export FBX on .blend save",
            default = True
            ) 
    
    bpy.utils.register_class(DirectlyToFBX)
    bpy.utils.register_class(SamTools_PT_MaterialColorPanel)
    bpy.utils.register_class(SamTools)
    bpy.utils.register_class(FixRotationAndScaleForUnity)
    
    #if not save_fbx in bpy.app.handlers.save_post:
        #bpy.app.handlers.save_post.append(save_fbx)
    
def unregister():
    bpy.utils.unregister_class(DirectlyToFBX)
    bpy.utils.unregister_class(SamTools_PT_MaterialColorPanel)
    bpy.utils.unregister_class(SamTools)
    bpy.utils.unregister_class(FixRotationAndScaleForUnity) 
    
    del bpy.types.Scene.fbx_foldername
    del bpy.types.Scene.auto_fix_scales
    del bpy.types.Scene.auto_fix_rotations
    
    #if save_fbx in bpy.app.handlers.save_post:
        #bpy.app.handlers.save_post.remove(save_fbx)

if __name__ == "__main__":
    register()